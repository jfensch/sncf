import numpy as np
import matplotlib.pyplot as plt
import itertools

##############
#### Functions
##############

## clef API SNCF
## c8cad8b9-4d2d-4b9d-b186-3eb224a1919b
## https://api.navitia.io/v1/coverage/sncf/pt_objects?type%5B%5D=line&q=Nevers-Paray&
## line Tours-Lyon: line:SNCF:FR:Line::9F274BE1-3158-4AD4-9B2F-5D1AC4DBCE72:
## line Nevers-Paray: line:SNCF:FR:Line::9E8A0A7B-FC96-452D-B101-08ACAC5E1547:
##

## IDGAIA: d94b368a-6667-11e3-89ff-01f464e0362d
## stop_points: stop_point:SNCF:87721787:LongDistanceTrain
#"name": "Nevers - Paray",


# https://api.navitia.io/v1/coverage/sncf/lines/line%3ASNCF%3AFR%3ALine%3A%3A9E8A0A7B-FC96-452D-B101-08ACAC5E1547%3A/departures?from_datetime=20220610T000000&data_freshness=realtime&count=100&
# http://www.xavierdupre.fr/app/ensae_teaching_cs/helpsphinx/notebooks/TD2A_eco_API_SNCF.html


#top=0.96,
#bottom=0.055,
#left=0.06,
#right=0.89,
#hspace=0.2,
#wspace=0.2

def minutes(time):
    """ Returns a time in minutes"""
    return 60*float(time.split('h')[0]) + float(time.split('h')[1])

def lateness(ETA, RTA):
    """ Returns the lateness in minutes """
    return minutes(RTA) - minutes(ETA)

def plot_bars(stats):
    """ Plots the desited statistics """

    on_time    = [100*sum(u<5 for u in stats[s])/len(stats[s]) for s in range(len(stats))]
    plus_5     = [100*sum(u>=5  and u<10  for u in stats[s])/len(stats[s]) for s in range(len(stats))]
    plus_10    = [100*sum(u>=10 and u<15  for u in stats[s])/len(stats[s]) for s in range(len(stats))]
    plus_15    = [100*sum(u>=15 and u<20  for u in stats[s])/len(stats[s]) for s in range(len(stats))]
    plus_20    = [100*sum(u>=20 and u<9999 for u in stats[s])/len(stats[s]) for s in range(len(stats))]
    annule     = [100*sum(u==9999 for u in stats[s])/len(stats[s]) for s in range(len(stats))]

    width = 0.5
    plt.bar(ETD, on_time, width, label='A l\'heure', color='darkgreen') #'royalblue') #, zorder=10)                                                                                              
    plt.bar(ETD, plus_5, width, label=r'Retard = 5min', color='limegreen',  bottom=on_time)
    plt.bar(ETD, plus_10, width, label=r'Retard = 10 min', color='gold',  bottom=np.array(plus_5)+np.array(on_time))
    plt.bar(ETD, plus_15, width, label=r'Retard = 15 min', color='orange',  bottom=np.array(plus_10)+ np.array(plus_5)+np.array(on_time))
    plt.bar(ETD, plus_20, width, label=r'Retard $\geqslant$ 20 min', color='crimson',  bottom=np.array(plus_15)+np.array(plus_10)+ np.array(plus_5)+np.array(on_time))
    plt.bar(ETD, annule, width, label=r'Annulés', color='black',  bottom=np.array(plus_20)+np.array(plus_15)+np.array(plus_10)+ np.array(plus_5)+np.array(on_time))
    
    plt.xlabel('Heure de départ') 
    plt.ylabel('Fraction [%]')

    plt.minorticks_on()
    plt.ylim(0,100)

#########
#### LOOP
#########
ETD = ['6h45', '8h10', '17h00', '18h07', '19h16']
ETA = ['7h33', '8h50', '17h39', '18h55', '20h01']
jours = ['lu', 'ma', 'me', 'je', 've']

stats   = []
stats_j = []

for jour in jours:
    stats_j.append([])

for train in range(len(ETA)):
    stats.append([])
    for jour in range(len(jours)):
        stats_j[jour].append([])

with open('Trajet.cat') as f:
    for line in f:
        if not '#' in line: #Skip header
            time_list = line.split()[2:]
            jour = line.split()[0]
            ind_j = jours.index(jour)

            for train in range(len(time_list)):
                if time_list[train] == 'nan':
                    stats[train].append(9999)
                    stats_j[ind_j][train].append(9999)
                else:    
                    stats[train].append(lateness(ETA[train], time_list[train]))
                    stats_j[ind_j][train].append(lateness(ETA[train], time_list[train]))

### Bar plot 
ncols=3
nrows=2
fig = plt.figure(figsize=(ncols*5 , (nrows+1)*5))

for iplot in range(ncols*nrows):

    axe = plt.subplot(nrows, ncols, iplot+1)
    if iplot == 0:
        plot_bars(stats)
        plt.legend(loc='center left', bbox_to_anchor=(3.4, -0.15))
        plt.title('Semaine')
    else:
        plot_bars(stats_j[iplot-1])
        title = 'Lundi'*(jours[iplot-1]=='lu') + \
        'Mardi'*(jours[iplot-1]=='ma') + \
        'Mercredi'*(jours[iplot-1]=='me') + \
        'Jeudi'*(jours[iplot-1]=='je') + \
        'Vendredi'*(jours[iplot-1]=='ve') 

        plt.title(title)


plt.subplots_adjust(left=0.05, right=0.875, top=0.95, bottom=0.1)
plt.show()

### Violin plot with the mediane and quartiles?



                
