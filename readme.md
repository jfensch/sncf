Requête pour trouver un endroit

```https://api.sncf.com/v1/coverage/sncf/places?q=perrache```

Id gare de Lyon Perrache : ```stop_area:SNCF:87722025```
Id gare de Bois d'Oingt Légny : ```stop_point:SNCF:87721787:LongDistanceTrain```

La requête pour récupérer tous les trains arrivés au bois d'Oingt le 09/06/2022

```
https://api.sncf.com/v1/coverage/sncf/stop_points/stop_point%3ASNCF%3A87721787%3ALongDistanceTrain/arrivals?from_datetime=20220609T000000&count=100
```
L'objet reçu contient ces propriétés :

![Objet JSON complet](/assets/img/objet_complet.png)

Ce qui nous intéresse pour l'instant c'est seulement la propriété arrivals qui est une lst comprenant les 8 arrivées de la journée au Bois d'Oingt.

À l'intérieur d'un arrival il y a ça :

![Objet JSON arrival](/assets/img/objet_arrival.png)

On est intéressés par stop_date_time, dans lequel il y a :

![Objet JSON arrival](/assets/img/objet_stop_date_time.png)

Il faut ensuite qu'on soustraie base_arrival_date_time (l'heure d'arrivée théorique) à arrival_date_time (l'heure d'arrivée effective) pour avoir le retard.

En pseudocode Python ça donnerait :

requeteAPI = requeteApiSncf()
arrivals = requeteAPI.arrivals

delays = []

for arrival in arrivals:
    delay = arrival.stop_date_time.arrival_date_time - arrival.stop_date_time.base_arrival_date_time
    delays.append(delay)

Et voilà avec ça on a la liste des retards pour tous les trains arrivés au Bois D'Oingt. 
Pour savoir à quel train cela correspond on pourrait le stocker sous forme de dictionnaire, la clé étant le base_arrival_date_time et la valeur étant le retard.
