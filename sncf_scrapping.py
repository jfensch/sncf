from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from datetime import datetime, timedelta
import urllib.request
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

######################
######################


def create_logs(train, code, date):
    print()
    print('Train:', train)
    browser = webdriver.Chrome(options=chrome_options)
    wait = WebDriverWait(browser, 20)

    url ='https://www.sncf.com/fr/itineraire-reservation/recherche-numero-train'

    browser.get(url)
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,"#CybotCookiebotDialogBodyLevelButtonLevelOptinDeclineAll"))).click()
    
    find_serial = browser.find_element(by="id", value="train-number")
    find_serial.send_keys('{}'.format(train))

    find_date = browser.find_element(by='id', value = 'search-date')
    find_date.clear() 
    find_date.send_keys('{}/{}/{}'.format(str(date.day).zfill(2), str(date.month).zfill(2), str(date.year)))  
    find_date.send_keys(Keys.ENTER) 
    
    wait.until(EC.visibility_of_element_located((By.ID, "miv-btn-toolbox-train-detail-modify")))

    html = browser.page_source
    soup = BeautifulSoup(html, 'lxml')

    logs = soup.find_all(['a', 'time'], ['link',  "itinerary-path-time"])
    #print(logs)
    browser.quit()

    return [s.text for s in logs]

def is_late(logs, target):
    if ':' in logs[ logs.index(target) -2] : # two times indicated             
        print("Retard à l'arrivée !")
        print('Heure prévue:', logs[ logs.index(target) -2])
        print('Heure réelle:',logs[ logs.index(target) -1])
    else:
        print("A l'heure")
    return

def write_in_file(res, file):
    return

######################
######################

chrome_options = Options()
chrome_options.add_argument("--headless")

trains_aller = ['893901', '893903', '893905']                                                 
trains_retour = ['893900', '893902', '893904']

codeCirculation_aller = ['24d4ab9c870ed6df6e457dcf0c7ef874', '48f8726471f1bcdac713707f20429d13', '37b0a888efccf73a1bf9503e46b27177']
codeCirculation_retour =['b9679dbc31ecf98792c1b83bf9f65e00', 'ffd2de3897e2080df5ba6c24e56be556', 'bf4ae92fa4817ef0ce212255e6e19fca']

target_aller = 'Lyon Perrache'
target_retour = "Bois-d'Oingt - Légny"

trains = []
codes = []
targets = []
for i in range(len(trains_aller)):
    trains.append(trains_aller[i])
    codes.append(codeCirculation_aller[i])
    targets.append(target_aller)
for i in range(len(trains_retour)):
    trains.append(trains_retour[i])
    codes.append(codeCirculation_retour[i])
    targets.append(target_retour)

days = ['lu', 'ma', 'me', 'je', 've']
res = ''

if __name__ == "__main__" :

    for date in [ datetime.now() - timedelta(1), datetime.now()]:
         day = datetime.weekday(date)
         if day > 4 : # week-end 
            print('Week-end')
            pass
         else:
            print(str(date).split()[0])
            res+='{}  {}.{}.{} '.format(days[day], str(date.day).zfill(2), str(date.month).zfill(2), str(date.year)[-2:])

            for i,train in enumerate(trains):
                logs = create_logs(train, codes[i], date)
                res += ' {} '.format(logs[ logs.index(targets[i]) -1])
                is_late(logs, targets[i])

         res += ' \n'
print()
print(res)
